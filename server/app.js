const express = require('express'); // web application framework
const helmet = require('helmet'); // Help secure Express apps with various HTTP headers
const cookieSession = require('cookie-session'); // stores the session data on the client within a cookie
const bodyParser = require('body-parser'); // body parsing middleware
const createDebug = require('debug'); // debugging utility
const logger = require('nmg-utils').logger({ consoleOn: false });

const debug = createDebug('nmg-boiler-template:app'); // create the debug object
const login = require('nmg-utils').login; // the login handler

const authenticationCtrl = require('./api/authentication/authentication.ctrl/authentication.ctrl');
const forgotPassCtrl = require('./api/user/user.ctrl/forgot.password');
const validateTokenCtrl = require('./api/user/user.ctrl/validate.token');
const resetForgottenPassCtrl = require('./api/user/user.ctrl/reset.forgotten.password');

const rds = require('nmg-utils').db.rds;

const parsedRequsetKeys = [
  'method',
  'originalUrl',
  'query',
  'headers',
  'session',
  'url',
  'user',
];
const parseRequest = (req = {}) => parsedRequsetKeys.reduce((o, i) => {
  o[i] = req[i];
  return o;
}, {});

const app = express(); // setup the web server

app.use((req, res, next) => {
  res.namogooStart = Date.now();
  next();
});
// add the cookie-session object
app.use(cookieSession({
  name: 'session',
  keys: ['qbm4dk1qz1u6z', 'aw2lalpcxr256'],
  cookie: {
    secure: process.env.NODE_ENV === 'production',
  },
}));
const namogooUserRegexp = /@namogoo\.com$/i;
const isNamogooUser = ({ username }) => username && namogooUserRegexp.test(username);

// use helmet
app.use(helmet());

// use bodyParser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// setup the authentication - after the session setup
login.setupAuthentication(app);

// setup the local login authentication
login.setupLocalAuthentication(app, async (username, password, done) => {
  try {
    if (!(await authenticationCtrl.isAuthenticated({ username, password }))) {
      logger.debug(`${username}: Unauthorized`, { username });
      return done(null, false, { message: 'Unauthorized' });
    }
    logger.debug(`${username}: Authorized`, { username });
    return done(null, { username });
  } catch (err) {
    logger.error(`${username}: Unauthorized`, { username, error: err });
    return done(null, false, { message: 'Unauthorized' });
  }
});

// setup the google login authentication
login.setupGoogleAuthentication(app, {
  clientID: '414789579884-s2oqpndq3oiivi1vv0medn4dn4bilthc.apps.googleusercontent.com',
  clientSecret: 'gRPILx45M-r147jIPuhanUKV',
  callbackURL: '/auth/google/callback',
  authenticateURL: '/auth/google',
  successRedirect: process.env.NODE_ENV === 'production' ? '/' : '//localhost:8080/',
  failureRedirect: process.env.NODE_ENV === 'production' ? '/login' : '//localhost:8080/',
  scope: ['profile', 'email'],
}, async (accessToken, refreshToken, profile, done) => {
  let user;
  try {
    debug(profile);
    user = { username: profile.emails && profile.emails.length > 0 && profile.emails[0].value };

    if (await authenticationCtrl.isAuthenticated({
      username: user.username,
      password: undefined,
      isApproved: true,
      isActive: true }) || isNamogooUser(user)) {
      logger.debug(`${user && user.username}: Authorized`, { username: user && user.username });
      done(null, user);
    }
    logger.debug(`${user && user.username}: Unauthorized`, { username: user && user.username });
    done(`${user && user.username}: Unauthorized`);
  } catch (err) {
    logger.error(`${user && user.username}: Unauthorized`, { username: user && user.username, error: err });
    done(`${user && user.username}: Unauthorized`);
  }
});

// the authentication function
const authentication = async (req, res, next) => {
  try {
    if (process.env.NODE_ENV === 'test') {
      return next();
    }

    if (!req.user) return res.status(401).send('Unauthorized');

    // debug(req.user, req.session, { isChanged: req.session.isChanged, isNew: req.session.isNew, isPopulated: req.session.isPopulated });
    // session was changed or is new or is empty
    if (req.session && (req.session.isChanged || req.session.isNew || !req.session.isPopulated)) {
    // check the user and get his userid and username
      if (!req.user) return res.status(401).send('Unauthorized - No User');
      if (!req.user.username) return res.status(401).send('Unauthorized - No Username');
    } else if (!req.user.username || !(await authenticationCtrl.isAuthenticated({
      username: req.user.username,
    }, {
      req: parseRequest(req),
    }))) {
      return res.status(401).send('Unauthorized - not Authenticated');
    }
    req.user.isNamogooUser = isNamogooUser(req.user);
    return next();
  } catch (err) {
    logger.error(`${req.user && req.user.username}: Unauthorized`, { username: req.user && req.user.username, error: err });
    return res.status(401).send('Unauthorized - Error');
  }
};

// the authoriztion function
const authoriztion = (req, res, next) => {
  if (false) {
    return res.status(401).send('Unauthorized');
  }
  return next();
};

/**
 * our REST API here
 */
app.use((req, res, next) => {
  debug(`Dispatching ${req.url} \nQuery: ${JSON.stringify(req.query)}`);
  const title = `>> ${req.user && req.user.username}: ${req.url}`;
  logger.debug(`CHP APP ${title}`, {
    url: req.url,
    queryParams: req.query,
    username: req.user && req.user.username,
  });
  req.on('end', () => {
    logger.debug(`CHP APP [END] ${title}`, {
      totalTime: Date.now() - res.namogooStart,
    });
  });

  res.rds = async (query, params, { message = '' } = {}) => rds.query(query, params, {
    message: `CHP APP @ RDS ${message ? `>> [${message}]` : ''}${title}`,
    req: parseRequest(req),
  });
  next();
});
app.use('/api', authentication, authoriztion, require('./api/index'));

// login post
app.post('/auth/login', login.getLocalStrategy(app), (req, res) => res.status(200).send('Authorized'));

// google login
app.get('/auth/google_login', login.getGoogleStrategy(app));

// logout and clear session
app.post('/auth/logout', (req, res) => {
  req.session = null;
  res.status(200).send('Logged out');
});

app.get('/auth/forgot-pass', forgotPassCtrl);
app.get('/auth/reset-forgot-pass', resetForgottenPassCtrl);
app.get('/auth/validate-token', validateTokenCtrl);

const aliveSince = new Date();
app.get('/auth/health-check', (req, res) => res.status(200).json({
  aliveSince,
  status: 'ok!',
}));

// express has executed all middleware functions and routes, and found that none of them responded, so we will return 404 Not Found
app.use((req, res, next) => {
  const err = new Error('Not Found');
  err.status = 404;
  logger.error(`${req.user && req.user.username}: ${req.url}`,
    { url: req.url, queryParams: req.query, username: req.user && req.user.username, error: err, status: 404 });
  next(err);
});

// error handler - error-handling middleware must be the last, after other app.use() and routes calls
// has four args: err, req, res, next
app.use((err, req, res) => {
  debug(err);
  logger.error(`${req.user && req.user.username}: ${req.url}`,
    { url: req.url, queryParams: req.query, username: req.user && req.user.username, error: err, status: 500 });
  res.status(500).send(err);
});

module.exports = app;
