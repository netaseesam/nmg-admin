const express = require('express');

const router = express.Router();

const getTitle = require('./overview.ctrl/page-title.get');

/**
 * Error Handler Wrapper
 * @param fn
 */
const errorHandlerWrapper = fn => (...args) => fn(...args).catch(args[2]);

// handling various type of requests
router.get('/get-title', errorHandlerWrapper(getTitle));

module.exports = router; // exporting router so that it can be used in app.js
