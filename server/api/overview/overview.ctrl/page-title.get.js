module.exports = async (req, res) => {
  const title = `Hello! this is the overview page! this is a demo page for the nmg-boilerplate.
  The overview page simulates an actual page in the system, and includes vuex use and server access.
   Feel free to use it's template for creating your own page.`;
  res.json({ data: title });
};
