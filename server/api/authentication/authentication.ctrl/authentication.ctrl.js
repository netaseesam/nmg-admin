const SHA256 = require('crypto-js/sha256');

const userCtrl = require('../../user/user.ctrl/user.ctrl');

const isAuthenticated = async ({ username, password } = {}, details = {}) => {
  const logObject = {
    ...details,
    message: `CHP APP @ RDS >> authentication check >> ${username}`,
    username,
  };
  const user = await userCtrl.getUser({ username }, logObject);
  // unknown user
  if (!user) {
    // add the user, but not approved it and not active
    return false;
  }

  return user.isApproved && user.isActive && (password ? SHA256(password).toString() === user.password : true);
};

module.exports = {
  isAuthenticated,
};
