const { db } = require('nmg-utils');

const rdsQuery = 'select token, created_at as createdAt from chp_app_user_reset_password_tokens_tbl where token = ?token';

const HOUR = 1000 * 60 * 60;
const TOKEN_EXPIRED_MS = HOUR * 24;

module.exports = async (req, res) => {
  try {
    const { query } = req;
    const { token } = query;
    const response = await db.rds.query(rdsQuery, { token });
    if (response.length === 0) {
      res.json({ ok: false, reason: 'Token not found' });
      return;
    }
    const { createdAt } = response[0];
    if ((+new Date(createdAt) + TOKEN_EXPIRED_MS) - Date.now() > 0) {
      // token did not expire yet, all good
      res.json({ ok: true });
    } else {
      res.json({ ok: false, reason: 'Token is expired' });
    }
  } catch (error) {
    res.send(error).status(500);
  }
};
