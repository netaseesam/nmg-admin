const logger = require('nmg-utils').logger();

module.exports = async ({ user, body }, res) => {
  res.status(200).send();
  logger.error(user, body);
};
