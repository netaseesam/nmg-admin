/* eslint max-len:0 */
const getHTMLTemplateByUrl = loginUrl => `
<div style="text-align:center;position: absolute;top: 0;left: 0;right: 0;bottom: 0;background-color:#fece3c;">
  <div style="padding:56px; font-weight:100; position: relative;width: 450px;display: inline-block;z-index: 3;height: 450px;top: calc(50% - 225px);background-color:white;margin-top: 20px;margin-bottom:20px;">
    <img src="http://stgdigitalinsights.namogoo.com/static/img/Namogoo_Logo_RGB_NoTagline_Black.png" class="logo-image">
    <p style="font-size: 23px; font-weight: 100; margin: 10px 0px 24px 0px;">RESET PASSWORD</p>

    <div>
      <div style="box-sizing: border-box;height: 2px;width: 111px;border: 1px solid #FECF3C;margin: 0 auto;"></div>
    </div>

    <p>We received a password reset request for your account.</p>
    <p>Click the button below to reset your password:</p>
    <br>
    <div>
      <div style="width: 395px;height: 40px;background-color: #F16327;color: #FFFFFF;margin: 0 auto;font-size: 14px;">
        <a style="display:block;height:100%;width:100%;line-height:40px; text-decoration: none; color: white; font-weight: 800" href="${loginUrl}">Reset Password</a>
      </div>
    </div>
    <br>
      <p>If you did not make this request,</p>
      <p>please contact customer support immediately.</p>
    <div>
      <div style="box-sizing: border-box;height: 2px;width: 111px;border: 1px solid #FECF3C;margin: 0 auto;"></div>
    </div>
    <p><a style="text-decoration: none" href="http://www.namogoo.com">namogoo.com</a></p>
  </div>
</div>
`;

module.exports = {
  getHTMLTemplateByUrl,
};
