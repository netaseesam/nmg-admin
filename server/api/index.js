const express = require('express');

/**
 * Error Handler Wrapper
 * @param fn
 */
global.errorHandlerWrapper = fn => (...args) => fn(...args).catch(args[2]);

// importing our handlers for the request
const user = require('./user/index');
const error = require('./error/index');
const overview = require('./overview/index');

const router = express.Router();

router.use('/user', user);
router.use('/overview', overview);
router.use('/error', error);

module.exports = router; // exporting router so that it can be used in app.js
