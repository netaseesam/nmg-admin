import { resetState } from '@/store/session/actions';

export default [
  resetState,
];
