export const validateToken = 'validateToken';
export const forgotPass = 'forgotPass';
export const resetPassAndLogin = 'resetPassAndLogin';
export const setForgotPassToken = 'setForgotPassToken';

export default {
  validateToken,
  forgotPass,
  resetPassAndLogin,
  setForgotPassToken,
};
