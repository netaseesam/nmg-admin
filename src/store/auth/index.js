import Api from '@/api';
import {
  SET_USERNAME_AND_PASS_TOKEN,
} from './mutations';

import {
  forgotPass,
  resetPassAndLogin,
  validateToken,
} from './actions';

const getAuthState = () => ({
  forgotPassToken: '',
  username: '',
});

// getters
const authGetters = {

};

// actions
const actions = {
  async [forgotPass](_, { username }) {
    return Api.forgotPass({ username });
  },
  async [resetPassAndLogin]({ state }, { password }) {
    const { username, forgotPassToken } = state;
    await Api.resetPass({ username, token: forgotPassToken, password });
    await Api.login({ username, password });
  },
  async [validateToken]({ commit }, { token, username }) {
    commit(SET_USERNAME_AND_PASS_TOKEN, { token, username });
    return Api.validateToken({ token });
  },
};

// mutations
const mutations = {
  [SET_USERNAME_AND_PASS_TOKEN](state, { token, username }) {
    state.forgotPassToken = token;
    state.username = username;
  },
};

export default {
  state: getAuthState(),
  getters: authGetters,
  actions,
  mutations,
};
