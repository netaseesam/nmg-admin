import Api from '@/api';

import {
  fetchOverviewTitle,
} from './actions';

import {
  SET_OVERVIEW_TITLE,
} from './mutations';

const getInitialState = () => ({
  overviewTitle: '',
});

// getters
const overviewGetters = {
  overviewTitle: state => state.overviewTitle,
};

// actions
const actions = {
  async fetchOverviewTitle({ commit }) {
    const response = await Api.fetchOverviewTitle();
    console.log('response', response);
    const title = response.body.data;
    commit(SET_OVERVIEW_TITLE, title);
  },
};

// mutations
const mutations = {
  [SET_OVERVIEW_TITLE](state, overviewTitle) {
    state.overviewTitle = overviewTitle;
  },
};

export default {
  namespaced: true,
  state: getInitialState(),
  getters: overviewGetters,
  actions,
  mutations,
};
