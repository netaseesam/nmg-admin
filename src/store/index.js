import Vue from 'vue';
import Vuex from 'vuex';
import session from './session';
import overview from './overview';
import auth from './auth';

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';

export default new Vuex.Store({
  modules: {
    auth,
    session,
    overview,
  },
  strict: debug,
});
