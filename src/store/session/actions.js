export const logout = 'logout';
export const login = 'login';
export const resetState = 'resetState';

export default {
  logout,
  login,
  resetState,
};
