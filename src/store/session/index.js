import Api from '@/api';
import logoutActions from '../helpers/logoutActions';

import {
  logout,
  login,
  resetState,
} from './actions';
import {
  SET_IS_NAMOGOO_USER,
  SET_AUTHENTICATED_STATUS,
  SET_INITIAL_SESSION_STATE,
} from './mutations';

const getInitialState = () => ({
  isAuthenticated: false,
  isNamogooUser: false,
});

// getters
const sessionGetters = {
  isAuthenticated: state => state.isAuthenticated,
  isNamogooUser: state => state.isNamogooUser,
};

// actions
const actions = {
  async [logout]({ dispatch }) {
    let response;
    try {
      response = await Api.logout();
      logoutActions.forEach(dispatch);
    } catch (error) {
      return response;
    }

    return response;
  },
  async [login](_, { username, password }) {
    const response = await Api.login({ username, password });
    return response;
  },
  [resetState]({ commit }) {
    commit(SET_INITIAL_SESSION_STATE);
  },
};

// mutations
const mutations = {
  [SET_IS_NAMOGOO_USER](state, isNamogooUser) {
    state.isNamogooUser = isNamogooUser;
  },
  [SET_AUTHENTICATED_STATUS](state, isAuthenticated) {
    state.isAuthenticated = isAuthenticated;
  },
  [SET_INITIAL_SESSION_STATE](state) {
    const initialState = getInitialState();
    Object.keys(initialState).forEach((key) => {
      state[key] = initialState[key];
    });
  },
};

export default {
  state: getInitialState(),
  getters: sessionGetters,
  actions,
  mutations,
};
