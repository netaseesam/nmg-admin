export const SET_IS_NAMOGOO_USER = 'SET_IS_NAMOGOO_USER';
export const SET_AUTHENTICATED_STATUS = 'SET_AUTHENTICATED_STATUS';
export const SET_INITIAL_SESSION_STATE = 'SET_INITIAL_SESSION_STATE';

export default {
  SET_IS_NAMOGOO_USER,
  SET_AUTHENTICATED_STATUS,
  SET_INITIAL_SESSION_STATE,
};
