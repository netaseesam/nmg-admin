import { Http } from 'vue-resource';

export default err => Http.post('api/error/post-error/', err);
