import fontawesome from '@fortawesome/fontawesome';
import faArrowUp from '@fortawesome/fontawesome-free-solid/faArrowUp';
import faArrowDown from '@fortawesome/fontawesome-free-solid/faArrowDown';
import faUserAlt from '@fortawesome/fontawesome-free-solid/faUserAlt';

const iconsToAdd = [
  faArrowDown,
  faArrowUp,
  faUserAlt,
];

fontawesome.library.add(...iconsToAdd);
