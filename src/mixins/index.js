import scrollTopOnMounted from './scrollTopOnMounted';

export default {
  scrollTopOnMounted,
};

export {
  scrollTopOnMounted,
};
