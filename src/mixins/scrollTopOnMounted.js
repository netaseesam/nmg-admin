export default {
  mounted() {
    const onNextTick = () => {
      const $elm = document.getElementById('viewcontainer');
      if ($elm) { $elm.scrollTop = 0; }
    };
    this.$nextTick(onNextTick);
  },
};
