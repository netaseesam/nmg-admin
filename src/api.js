import { Http } from 'vue-resource';
import router from '@/router';
import store from '@/store';
import * as sessionActions from '@/store/session/actions';
import errorHandler from './utils/errorHandler';

const onError = async (error) => {
  const { status } = error;
  const { isAuthenticated } = store.getters;
  const requestIsUnauthorized = status === 401;
  if (requestIsUnauthorized && isAuthenticated) {
    await store.dispatch(sessionActions.logout);
    router.push('/login');
  }
  errorHandler(error);
};

const handlerWrapper = (promise) => {
  promise.catch(onError);
  return promise;
};

class API {
  static login({ username, password }) {
    return API.post('auth/login', { username, password });
  }
  static logout() {
    return API.post('auth/logout');
  }
  static forgotPass({ username }) {
    return API.get('auth/forgot-pass', { username });
  }
  static resetPass({ username, token, password }) {
    return API.get('auth/reset-forgot-pass', { username, token, password });
  }
  static validateToken({ token }) {
    return API.get('auth/validate-token', { token });
  }
  static fetchOverviewTitle() {
    return API.get('api/overview/get-title');
  }
  static get(url, params) {
    return handlerWrapper(Http.get(url, { params }));
  }
  static post(url, params) {
    return handlerWrapper(Http.post(url, params));
  }
}

export default API;
