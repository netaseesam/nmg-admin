import Vue from 'vue';
import Router from 'vue-router';
import LayoutContent from '@/components/LayoutContent';
import Overview from '@/pages/Overview/';
import Login from '@/pages/Login/';

Vue.use(Router);

const requiresAuth = {
  meta: {
    requiresAuth: true,
  },
};

const router = new Router({
  routes: [
    {
      path: '/',
      component: LayoutContent,
      children: [
        {
          path: '',
          name: 'Overview',
          component: Overview,
          ...requiresAuth,
        },
      ],
    },
    {
      path: '/login',
      name: 'Login',
      component: Login,
      ...requiresAuth,
    },
  ],
});


export default router;
