// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import VueResource from 'vue-resource';
import 'normalize.css';
import nmgComponents from 'nmg-components';
import FontAwesomeIcon from '@fortawesome/vue-fontawesome';
import './fontawesome';
import App from './App';
import store from './store';
import router from './router';
import Filters from './utils/filters';

import './styles/global.scss';

Vue.use(VueResource);
Vue.use(Filters);

Vue.component('FontAwesomeIcon', FontAwesomeIcon);

const registerNmgComponents = (lib) => {
  Object.keys(lib).forEach((componentName) => {
    const component = lib[componentName];
    const name = componentName.replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase().replace('_', '-');
    const componentKebabCaseName = `nmg-${name}`;
    Vue.component(componentKebabCaseName, component);
  });
};

// Register goomponents dynamically
registerNmgComponents(nmgComponents);

Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  store,
  template: '<App/>',
});
